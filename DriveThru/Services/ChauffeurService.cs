﻿using DriveThru.Models;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;

namespace DriveThru.Services
{
    public class ChauffeurService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="mongodb"></param>
        public ChauffeurService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// Chauffeur Register
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel ChauffeurRegister(ChauffeurModelsIn req, Int64 lang_id)
        {
            try
            {
                var m_chauffeur = this._mongodb.GetCollection<m_chauffeur>(typeof(m_chauffeur).Name);
                UserModelsOut output_model = new UserModelsOut();
                if (req != null)
                {
                    var ch = m_chauffeur.Find(y => y.id_card == req.id_card && y.inactive == false && y.locked == false).ToList();
                    if (ch.Count > 0)
                    {
                        OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.CHAUFFEUR_DUPLICATE, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }

                    m_chauffeur m_ = new m_chauffeur()
                    {
                        addr = req.addr,
                        ch_id = this.GetIncrement(typeof(m_chauffeur).Name),
                        ch_name = string.Format("{0} {1}", req.f_name, req.l_name),
                        company_id = Company.DEMO,
                        createAt = req.user_add,
                        create_date = DateTime.Now,
                        date_of_birth = req.date_of_birth,
                        editAt = req.user_add,
                        edit_date = DateTime.Now,
                        email = req.email,
                        f_name = req.f_name,
                        id_card = req.id_card,
                        inactive = false,
                        locked = false,
                        l_name = req.l_name,
                        mobile = req.mobile
                    };

                    m_chauffeur.InsertOne(m_);

                    output_model.ErrorCode = ErrorCode.SUCCESS;
                    output_model.ErrorSubCode = ErrorCode.SUCCESS;
                }
                else
                {   // ข้อมูล input ไม่ถูกต้อง
                    OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// User Info
        /// </summary>
        /// <param name="ch_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurInfoModelsOut ChauffeurInfo(Int64 ch_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                var m_chauffeur = this._mongodb.GetCollection<m_chauffeur>(typeof(m_chauffeur).Name);
                ChauffeurInfoModelsOut output_model = new ChauffeurInfoModelsOut();
                if (ch_id > 0 || company_id > 0)
                {
                    List<m_chauffeur> info_chauffeur = m_chauffeur.Find(t => t.ch_id == ch_id && t.inactive == false && t.locked == false).ToList();
                    if (info_chauffeur.Count > 0)
                    {
                        m_chauffeur info = info_chauffeur.First();
                        if (info.locked == true)
                        { //  ถูก locked การใช้งาน
                            OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.CHAUFFEUR_LOCKED, lang_id, "");
                            output_model.ErrorCode = err.ErrorCode;
                            output_model.ErrorSubCode = err.ErrorSubCode;
                            output_model.Title = err.Title;
                            output_model.ErrorMesg = err.ErrorMesg;
                            output_model.ErrorDetail = err.ErrorDetail;
                            return output_model;
                        }

                        output_model.Data = new ChauffeurInfoModels()
                        {
                           addr = info.addr,
                           ch_id = info.ch_id,
                           ch_name = info.ch_name,
                           company_id = info.company_id,
                           createAt = info.createAt,
                           create_date = info.create_date,
                           date_of_birth = info.date_of_birth,
                           editAt = info.editAt,
                           edit_date = info.edit_date,
                           email = info.email,
                           f_name = info.f_name,
                           id_card = info.id_card,
                           inactive = info.inactive,
                           l_name = info.l_name,
                           mobile = info.mobile
                        };

                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else
                    { // หา CHAUFFEUR ไม่เจอ
                        OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.NO_DATA_CHAUFFEUR, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                { // ระบุบ ch_id ไม่ถูก
                    OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// Chauffeur List
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurListModelsOut ChauffeurList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                var m_chauffeur = this._mongodb.GetCollection<m_chauffeur>(typeof(m_chauffeur).Name);
                ChauffeurListModelsOut output_model = new ChauffeurListModelsOut();

                List<m_chauffeur> info_chauffeur = m_chauffeur.Find(t => t.locked == false && t.inactive == false && t.company_id == company_id).ToList();
                if (info_chauffeur.Count > 0)
                {
                    output_model.Data = info_chauffeur.Select(s => new ChauffeurInfoModels()
                    {
                        company_id = s.company_id,
                        createAt = s.createAt,
                        create_date = s.create_date,
                        editAt = s.editAt,
                        edit_date = s.edit_date,
                        inactive = s.inactive,
                        addr = s.addr,
                        ch_id = s.ch_id,
                        ch_name = s.ch_name,
                        date_of_birth = s.date_of_birth,
                        email = s.email,
                        f_name = s.f_name,
                        id_card = s.id_card,
                        l_name = s.l_name,
                        mobile = s.mobile

                    }).ToList();
                    output_model.ErrorCode = ErrorCode.SUCCESS;
                    output_model.ErrorSubCode = ErrorCode.SUCCESS;
                }
                else {
                    output_model.Data = new List<ChauffeurInfoModels>();
                }
              
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// Delete Chauffeur
        /// </summary>
        /// <param name="ch_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel DeleteChauffeur(Int64 ch_id, Int64 lang_id)
        {
            try
            {
                var m_chauffeur = this._mongodb.GetCollection<m_chauffeur>(typeof(m_chauffeur).Name);
                OutputBaseModel output_model = new OutputBaseModel();

                if (ch_id > 0)
                {
                    var ch = m_chauffeur.Find(u => u.ch_id == ch_id && u.locked == false && u.inactive == false).ToList();
                    if (ch.Count == 1)
                    {
                        var filter = Builders<m_chauffeur>.Filter.Eq(o => o.ch_id, ch_id);
                        var update = Builders<m_chauffeur>.Update.Set(o => o.locked, true).Set(o => o.inactive, true);
                        m_chauffeur.FindOneAndUpdate<m_chauffeur>(filter, update);
                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else
                    {
                        OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.NO_DATA_CHAUFFEUR, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                {
                    OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }
    }
}