﻿using DriveThru.Models;
using DriveThru.Models.MongoDataBase;
using DriveThru.Utilitys;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;
using System.Web;

namespace DriveThru.Services
{
    public class MongoSystemService
    {
        /// <summary>
        /// เชื่อมต่อฐานข้อมูล
        /// </summary>
        public IMongoDatabase _mongodb { get; set; }

        /// <summary>
        /// เรียก sequence name
        /// </summary>
        /// <param name="sequence_name">sequence name</param>
        public Int64 GetIncrement(string sequence_name)
        {
            try
            {

                var colle = _mongodb.GetCollection<counters>(typeof(counters).Name);
                counters auto_inc = new counters();
                List<counters> cou = colle.Find(u => u.t_name == sequence_name).ToList();
                if (cou.Count == 1)
                {
                    counters info_counters = cou.First();
                    info_counters.seq += 1;
                    var query = Builders<counters>.Filter.Eq(f => f.t_name, sequence_name);
                    var update = Builders<counters>.Update.Set(o => o.seq, info_counters.seq);
                    var opts = new FindOneAndUpdateOptions<counters>()
                    {
                        ReturnDocument = ReturnDocument.After
                    };
                    auto_inc = colle.FindOneAndUpdate(query, update, opts);
                }
                else
                {
                    auto_inc = new counters()
                    {
                        seq = 1,
                        t_name = sequence_name
                    };
                    colle.InsertOne(auto_inc);
                }
                if (auto_inc == null) throw new Exception("Not Increment");
                return auto_inc.seq;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error_code"></param>
        /// <param name="sub_error_code"></param>
        /// <param name="lang_id"></param>
        /// <param name="plus_mesg"></param>
        /// <returns></returns>
        public OutputBaseModel GetException(int error_code, int sub_error_code, Int64 lang_id, string plus_mesg)
        {
            OutputBaseModel output = new OutputBaseModel()
            {
                ErrorCode = error_code,
                ErrorSubCode = sub_error_code
            };
            try
            {
                #region Collection
                IMongoCollection<zz_error_message> colle = this._mongodb.GetCollection<zz_error_message>(typeof(zz_error_message).Name);
                #endregion

                #region Query
                List<zz_error_message> query = null;
                zz_error_message error_message = null;
                query = colle.Find(i => i.err_id == error_code && i.err_sub == sub_error_code).ToList();
                #endregion

                #region Output
                if (query.Count > 0)
                {
                    error_message = query.First();
                    if (lang_id == Language.TH)
                    {
                        output.Title = error_message.title2;
                        output.ErrorMesg = error_message.err_msg2;
                        output.ErrorDetail = string.Format("{0} {1}", error_message.err_fix2, plus_mesg);
                    }
                    else if (lang_id == Language.EN)
                    {
                        output.Title = error_message.title1;
                        output.ErrorMesg = error_message.err_msg1;
                        output.ErrorDetail = string.Format("{0} {1}", error_message.err_fix1, plus_mesg);
                    }
                }
                else
                {
                    query = colle.Find(i => i.err_id == 100 && i.err_sub == -1).ToList();
                    if (query.Count > 0)
                    {
                        error_message = query.First();
                        output.Title = error_message.title1;
                        output.ErrorMesg = error_message.err_msg2;
                        output.ErrorDetail = string.Format("{0} {1}", error_message.err_fix2, plus_mesg);
                    }
                    else
                    {
                        output.Title = "ความผิดพลาด";
                        output.ErrorMesg = "ไม่สามารถระบุความผิดพลาดได้";
                        output.ErrorDetail = string.Format("{0} {1}", "ไม่สามารถระบุความผิดพลาดได้ กรุณาติดต่อผู้ดูแลระบบของท่าน", plus_mesg);
                    }
                }
                return output;
                #endregion
            }
            catch (Exception exce)
            {
                throw exce;
            }
        }

        /// <summary>
        /// log User
        /// </summary>
        /// <param name="user_id">ผู้ใช้งาน</param>
        /// <param name="is_type">รหัส type (1=User login // 2=User logout)</param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        public void isLogUserLoginLogout(Int64 user_id,Int64 is_type,Int64 company_id)
        {
            try
            {
                if (is_type == log_user_type.USER_ONLINE || is_type == log_user_type.USER_OFFLINE) // log user 
                {
                    var t_log_user = this._mongodb.GetCollection<t_log_user>(typeof(t_log_user).Name);
                    var zz_log_user_type = this._mongodb.GetCollection<zz_log_user_type>(typeof(zz_log_user_type).Name);
                    var info = zz_log_user_type.Find(r => r.log_type_user_id == is_type && r.inactive == false).FirstOrDefault();
                    t_log_user log = new t_log_user()
                    {
                        company_id = company_id,
                        createAt = user_id,
                        create_date = DateTime.Now,
                        editAt = user_id,
                        edit_date = DateTime.Now,
                        inactive = false,
                        log_id = this.GetIncrement(typeof(t_log_user).Name),
                        log_type_user_id = info.log_type_user_id,
                        log_type_user_name = info.log_type_user_name
                    };
                    t_log_user.InsertOneAsync(log);
                }
            } 
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// log Position
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="is_type"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        public void isLogUserPosition(Int64 user_id, Int64 is_type, Int64 company_id, positions req)
        {
            try
            {
                if (is_type == log_user_type.USER_POSITION || is_type == log_user_type.CHAUFFEUE_POSITION) // log Position 
                {
                    if (req != null)
                    {
                        var t_log_position = this._mongodb.GetCollection<t_log_position>(typeof(t_log_position).Name);
                        var zz_log_user_type = this._mongodb.GetCollection<zz_log_user_type>(typeof(zz_log_user_type).Name);
                        var info = zz_log_user_type.Find(r => r.log_type_user_id == is_type && r.inactive == false).FirstOrDefault();
                        t_log_position log = new t_log_position()
                        {
                            company_id = company_id,
                            createAt = user_id,
                            create_date = DateTime.Now,
                            editAt = user_id,
                            edit_date = DateTime.Now,
                            inactive = false,
                            log_id = this.GetIncrement(typeof(t_log_user).Name),
                            log_type_user_id = info.log_type_user_id,
                            log_type_user_name = info.log_type_user_name,
                            positions = new positions()
                            {
                                details = req.details,
                                latitude = req.latitude,
                                longitude = req.longitude
                            }
                        };
                        t_log_position.InsertOneAsync(log);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}