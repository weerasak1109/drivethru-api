﻿using DriveThru.Models;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static DriveThru.Models.MongoDataBase.CarModels;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;

namespace DriveThru.Services
{
    public class CarService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="mongodb"></param>
        public CarService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// Car Register
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel CarRegister(CarModelsIn req, Int64 lang_id)
        {
            try
            {
                var m_car = this._mongodb.GetCollection<m_car>(typeof(m_car).Name);
                UserModelsOut output_model = new UserModelsOut();
                if (req != null)
                {
                   var ch = m_car.Find(y => y.license_plate == req.license_plate.Trim() && y.company_id == req.company_id).ToList();
                    if (ch.Count > 0) 
                    {
                        OutputBaseModel err = this.GetException(ErrorCode.JOB, Car.CAR_DUPLICATE, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }

                    m_car m_ = new m_car()
                    {
                        car_brand = req.car_brand,
                        car_color = req.car_color,
                        car_id = this.GetIncrement(typeof(m_car).Name),
                        car_model = req.car_model,
                        company_id = req.company_id,
                        createAt = req.createAt,
                        create_date = DateTime.Now,
                        editAt = req.createAt,
                        edit_date = DateTime.Now,
                        inactive = false,
                        license_plate = req.license_plate,
                        locked = false
                    };
                    m_car.InsertOne(m_);

                    output_model.ErrorCode = ErrorCode.SUCCESS;
                    output_model.ErrorSubCode = ErrorCode.SUCCESS;
                }
                else
                {   // ข้อมูล input ไม่ถูกต้อง
                    OutputBaseModel err = this.GetException(ErrorCode.CAR, Car.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// User Info
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public CarInfoModelsOut CarInfo(Int64 car_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                var m_car = this._mongodb.GetCollection<m_car>(typeof(m_car).Name);
                CarInfoModelsOut output_model = new CarInfoModelsOut();
                if (car_id > 0 || company_id > 0)
                {
                    List<m_car> info_car = m_car.Find(t => t.car_id == car_id && t.inactive == false && t.company_id == company_id).ToList();
                    if (info_car.Count > 0)
                    {
                        m_car info = info_car.First();
                        if (info.locked == true)
                        { //  ถูก locked การใช้งาน
                            OutputBaseModel err = this.GetException(ErrorCode.CAR, Car.CAR_LOCKED, lang_id, "");
                            output_model.ErrorCode = err.ErrorCode;
                            output_model.ErrorSubCode = err.ErrorSubCode;
                            output_model.Title = err.Title;
                            output_model.ErrorMesg = err.ErrorMesg;
                            output_model.ErrorDetail = err.ErrorDetail;
                            return output_model;
                        }
                        
                        output_model.Data = new CarInfoModels()
                        {
                           car_brand = info.car_brand,
                           car_color = info.car_color,
                           car_id = info.car_id,
                           car_model = info.car_model,
                           company_id = info.company_id,
                           createAt = info.createAt,
                           create_date = info.create_date,
                           editAt = info.editAt,
                           edit_date = info.edit_date,
                           inactive = info.inactive,
                           license_plate = info.license_plate,
                           locked = info.locked
                        };

                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else
                    { // หา car ไม่เจอ
                        OutputBaseModel err = this.GetException(ErrorCode.CAR, Car.NO_DATA_CAR, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                { // ระบุบ user_id ไม่ถูก
                    OutputBaseModel err = this.GetException(ErrorCode.CAR, Car.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// Car List
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public CarListModelsOut CarList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                var m_car = this._mongodb.GetCollection<m_car>(typeof(m_car).Name);
                CarListModelsOut output_model = new CarListModelsOut();

                List<m_car> info_car = m_car.Find(t => t.locked == false && t.inactive == false && t.company_id == company_id).ToList();
                if (info_car.Count > 0)
                {
                   output_model.Data = info_car.Select(s => new CarInfoModels()
                    { 
                        car_brand = s.car_brand,
                        car_color = s.car_color,
                        car_id = s.car_id,
                        car_model = s.car_model,
                        company_id = s.company_id,
                        createAt = s.createAt,
                        create_date = s.create_date,
                        editAt = s.editAt,
                        edit_date = s.edit_date,
                        inactive = s.inactive,
                        license_plate = s.license_plate,
                        locked = s.locked
                     }).ToList();
                    output_model.ErrorCode = ErrorCode.SUCCESS;
                    output_model.ErrorSubCode = ErrorCode.SUCCESS;
                }
                else
                {
                    output_model.Data = new List<CarInfoModels>();
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// Delete Car
        /// </summary>
        /// <param name="car_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel DeleteCar(Int64 car_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                var m_car = this._mongodb.GetCollection<m_car>(typeof(m_car).Name);
                OutputBaseModel output_model = new OutputBaseModel();

                if (car_id > 0 && company_id > 0)
                {
                    var ch = m_car.Find(u => u.car_id == car_id && u.locked == false && u.inactive == false && u.company_id == company_id).ToList();
                    if (ch.Count == 1)
                    {
                        var filter = Builders<m_car>.Filter.Eq(o => o.car_id, car_id);
                        var update = Builders<m_car>.Update.Set(o => o.locked, true).Set(o => o.inactive, true);
                        m_car.FindOneAndUpdate<m_car>(filter, update);
                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else 
                    {
                        OutputBaseModel err = this.GetException(ErrorCode.CAR, Car.NO_DATA_CAR, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                { 
                    OutputBaseModel err = this.GetException(ErrorCode.CAR, Car.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }
    }
}