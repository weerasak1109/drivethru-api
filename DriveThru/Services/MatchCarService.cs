﻿using DriveThru.Models;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;

namespace DriveThru.Services
{
    public class MatchCarService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="mongodb"></param>
        public MatchCarService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// Match คนขับ กับ รถ
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel MatchChauffeurCar(MatchChauffeurCarModelIn req,Int64 lang_id)
        {
            OutputBaseModel output_model = new OutputBaseModel();
            try
            {
                if (req == null || req.chauffeurs == null || req.chauffeurs.Count < 0 || req.car_id == 0 || req.company_id < 0) 
                {
                    OutputBaseModel err = this.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, MChauffeurCar.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                var m_approved_car = this._mongodb.GetCollection<m_approved_car>(typeof(m_approved_car).Name);
                var m_car = this._mongodb.GetCollection<m_car>(typeof(m_car).Name);

                var car_info = m_car.Find(t => t.car_id == req.car_id && t.inactive == false && t.locked == false && t.company_id == req.company_id).ToList();
                if (car_info.Count > 0)
                {
                   m_car info = car_info.First();
                   m_approved_car approved = new m_approved_car()
                    {
                        inactive = false,
                        approved = true,
                        approvedAt = 0,
                        approved_date = null,
                        app_car_id = this.GetIncrement(typeof(m_approved_car).Name),
                        car_brand = info.car_brand,
                        car_color = info.car_color,
                        car_id = req.car_id,
                        car_model = info.car_model,
                        company_id = info.company_id,
                        createAt = req.user_add,
                        create_date = DateTime.Now,
                        editAt = req.user_add,
                        edit_date = DateTime.Now,
                        license_plate = info.license_plate,
                        chauffeurs = new List<chauffeurs>()
                    };

                    approved.chauffeurs = req.chauffeurs;

                    m_approved_car.InsertOne(approved);
                }
                else
                { // หาคนขับไม่เจอ หรือคนขับถูก locked
                    OutputBaseModel err = this.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, MChauffeurCar.NO_DATA_CHAUFFEUE_OR_CAR_LOCKED, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
              
                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// ข้อมูล คนขับ กับ รถ info
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurCarModelInfoOut ChauffeurCarinfo(Int64 app_car_id ,Int64 company_id, Int64 lang_id)
        {
            ChauffeurCarModelInfoOut output_model = new ChauffeurCarModelInfoOut();
            try
            {
                if (app_car_id == 0 || company_id < 0)
                {
                    OutputBaseModel err = this.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, MChauffeurCar.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                var m_approved_car = this._mongodb.GetCollection<m_approved_car>(typeof(m_approved_car).Name);

                var approved_car_info = m_approved_car.Find(r => r.app_car_id == app_car_id && r.approved == true && r.inactive == false && r.company_id == company_id).ToList();
                if (approved_car_info.Count > 0)
                {
                    var info = approved_car_info.First();
                    output_model.Data = new ChauffeurCarModelInfo()
                    {
                        approved = info.approved,
                        approvedAt = info.approvedAt,
                        approved_date = info.approved_date,
                        app_car_id = info.app_car_id,
                        car_brand = info.car_brand,
                        car_color = info.car_color,
                        car_id = info.car_id,
                        car_model = info.car_model,
                        license_plate = info.license_plate,
                        company_id = info.company_id,
                        chauffeurs = new List<chauffeurs>()
                    };

                    foreach (var row in info.chauffeurs)
                    {
                        chauffeurs chauf = new chauffeurs()
                        {
                            ch_id = row.ch_id,
                            ch_name = row.ch_name,
                            email = row.email,
                            f_name = row.f_name,
                            l_name = row.l_name,
                            mobile = row.mobile
                        };
                        output_model.Data.chauffeurs.Add(chauf);
                    }
                }
                else 
                {
                    OutputBaseModel err = this.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, MChauffeurCar.NO_DATA_MATCH, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// ข้อมูล คนขับ กับ รถ list
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurCarModelListOut ChauffeurCarList( Int64 company_id, Int64 lang_id)
        {
            ChauffeurCarModelListOut output_model = new ChauffeurCarModelListOut();
            try
            {
                if (company_id < 0)
                {
                    OutputBaseModel err = this.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, MChauffeurCar.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                var m_approved_car = this._mongodb.GetCollection<m_approved_car>(typeof(m_approved_car).Name);

                var list = m_approved_car.Find(r => r.approved == true && r.inactive == false && r.company_id == company_id).ToList();
                if (list.Count > 0)
                {
                    output_model.Data = new List<ChauffeurCarModelInfo>();
                    foreach (var row in list)
                    {
                        ChauffeurCarModelInfo data = new ChauffeurCarModelInfo()
                        {
                            approved = row.approved,
                            approvedAt = row.approvedAt,
                            approved_date = row.approved_date,
                            app_car_id = row.app_car_id,
                            car_brand = row.car_brand,
                            car_color = row.car_color,
                            car_id = row.car_id,
                            car_model = row.car_model,
                            license_plate = row.license_plate,
                            company_id = row.company_id,
                            chauffeurs = new List<chauffeurs>()
                        };
                        data.chauffeurs = row.chauffeurs;
                        output_model.Data.Add(data);
                    }
                }
                else
                {
                    OutputBaseModel err = this.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, MChauffeurCar.NO_DATA_MATCH, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }
    }
}