﻿using DriveThru.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;
using System.Web;
using DriveThru.Utilitys;

namespace DriveThru.Services
{
    public class JobService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="mongodb"></param>
        public JobService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        public OutputBaseModel CreateJob(JobModelsIn req, Int64 lang_id)
        {
            try
            {
                OutputBaseModel output_model = new OutputBaseModel();

                if (req == null) 
                { //input error
                    OutputBaseModel err = this.GetException(ErrorCode.JOB, Job.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                var m_job = this._mongodb.GetCollection<m_job>(typeof(m_job).Name);
                m_job job = new m_job() 
                {
                    approved = true,
                    approvedAt = req.user_id,
                    approved_date = DateTime.Now,
                    company_id = Company.DEMO,
                    createAt = req.user_id,
                    create_date = DateTime.Now,
                    editAt = req.user_id,
                    edit_date = DateTime.Now,
                    e_terminal = req.e_terminal,
                    inactive = false,
                    job_detail = req.job_detail,
                    job_id = this.GetIncrement(typeof(m_job).Name),
                    job_name = req.job_name,
                    s_terminal = req.s_terminal,
                    ticket_price = req.ticket_price,
                    user_id = req.user_id,
                    carslists = new List<carslist>(),
                    positions = new List<positions>()
                };
                job.positions = req.positions;
                job.carslists = req.carslists;
                m_job.InsertOne(job);

                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;

                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public JobModelsOutList JobList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                JobModelsOutList output_model = new JobModelsOutList();
                if (company_id < 0)
                {
                    OutputBaseModel err = this.GetException(ErrorCode.JOB, Job.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                var m_job = this._mongodb.GetCollection<m_job>(typeof(m_job).Name);

                List<m_job> job = m_job.Find(r => r.company_id == company_id && r.approved == true && r.inactive == false).ToList();
                if (job.Count > 0)
                {
                    output_model.Data = new List<JobModelsOut>();
                    foreach (var row in job)
                    {
                        JobModelsOut info = new JobModelsOut()
                        {
                            carslists = new List<carslist>(),
                            company_id = row.company_id,
                            e_terminal = row.e_terminal,
                            job_detail = row.job_detail,
                            job_id = row.job_id,
                            job_name = row.job_name,
                            positions = new List<positions>(),
                            s_terminal = row.s_terminal,
                            ticket_price = row.ticket_price
                        };
                        info.carslists = row.carslists;
                        info.positions = row.positions;
                        output_model.Data.Add(info);
                    };
                    
                }
                else 
                {
                    OutputBaseModel err = this.GetException(ErrorCode.JOB, Job.NO_DATA_JOB, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;

                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="job_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public JobModelsOutInfo JobInfo(Int64 job_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                JobModelsOutInfo output_model = new JobModelsOutInfo();
                if (job_id < 0 || company_id < 0)
                {
                    OutputBaseModel err = this.GetException(ErrorCode.JOB, Job.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                var m_job = this._mongodb.GetCollection<m_job>(typeof(m_job).Name);

                List<m_job> job = m_job.Find(r => r.job_id == job_id && r.company_id == company_id && r.approved == true && r.inactive == false).ToList();
                if (job.Count > 0)
                {
                    m_job info = job.First();
                    output_model.Data = new JobModelsOut()
                    {
                        carslists = new List<carslist>(),
                        company_id = info.company_id,
                        e_terminal = info.e_terminal,
                        job_detail = info.job_detail,
                        job_id = info.job_id,
                        job_name = info.job_name,
                        positions = new List<positions>(),
                        s_terminal = info.s_terminal,
                        ticket_price = info.ticket_price
                    };
                    output_model.Data.positions = info.positions;
                    output_model.Data.carslists = info.carslists;

                    output_model.ErrorCode = ErrorCode.SUCCESS;
                    output_model.ErrorSubCode = ErrorCode.SUCCESS;
                }
                else 
                {
                    OutputBaseModel err = this.GetException(ErrorCode.JOB, Job.NO_DATA_JOB, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }

                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }
    }
}