﻿using DriveThru.Models;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;

namespace DriveThru.Services
{
    public class UserService : MongoSystemService
    {
        /// <summary>
        /// Tracking logic constructor
        /// </summary>
        /// <param name="mongodb"></param>
        public UserService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// เปลี่ยนสถานะ online
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        private int online(Int64 user_id, Int64 company_id)
        {
            try
            {
                var z_user = this._mongodb.GetCollection<z_user>(typeof(z_user).Name);
                if (user_id > 0)
                {
                    var user = z_user.Find(u => u.user_id == user_id && u.company_id == company_id).FirstOrDefault();
                    if (user != null) 
                    {
                        user.online.is_online = true;
                        user.online.online_datetime = DateTime.Now;
                        var up = z_user.FindOneAndReplaceAsync(u => u.user_id == user_id, user);
                    }
                }
                else 
                {
                    return ErrorCode.ERROR_STATUS_ONLINE;
                }

                return ErrorCode.SUCCESS;
            }
            catch(Exception ex) 
            {
                throw ex;
            }
        }

        /// <summary>
        /// เปลี่ยนสถานะ offline
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        private int offline(Int64 user_id, Int64 company_id)
        {
            try
            {
                var z_user = this._mongodb.GetCollection<z_user>(typeof(z_user).Name);
                if (user_id > 0)
                {
                    var user = z_user.Find(u => u.user_id == user_id && u.company_id == company_id).FirstOrDefault();
                    if (user != null) 
                    {
                        user.online.is_online = false;
                        user.online.offline_datetime = DateTime.Now;
                        var up = z_user.FindOneAndReplaceAsync(u => u.user_id == user_id, user);
                    }
                }
                else 
                {
                    return ErrorCode.ERROR_STATUS_ONLINE;
                }

                return ErrorCode.SUCCESS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserModelsOut Register(UserModelsIn req, Int64 lang_id)
        {
            try
            {
                var z_user = this._mongodb.GetCollection<z_user>(typeof(z_user).Name);
                UserModelsOut output_model = new UserModelsOut();
                if (req != null)
                {
                    if (req.user_type == 1) // เช็ค คนขับ
                    {
                        var m_chauffeur = this._mongodb.GetCollection<m_chauffeur>(typeof(m_chauffeur).Name);
                        var ch = m_chauffeur.Find(y => y.id_card == req.id_card && y.inactive == false && y.locked == false).ToList();
                        if (ch.Count > 0)
                        {
                            OutputBaseModel err = this.GetException(ErrorCode.CHAUFFEUR, Chauffeur.CHAUFFEUR_DUPLICATE, lang_id, "");
                            output_model.ErrorCode = err.ErrorCode;
                            output_model.ErrorSubCode = err.ErrorSubCode;
                            output_model.Title = err.Title;
                            output_model.ErrorMesg = err.ErrorMesg;
                            output_model.ErrorDetail = err.ErrorDetail;
                            return output_model;
                        }
                    }
                   List<z_user> ch_user = z_user.Find(t => t.username == req.username && t.inactive == false && t.locked == false && t.activated == true).ToList();
                    if (ch_user.Count > 0) 
                    { // มี user นี้ในระบบแล้ว
                        OutputBaseModel err = this.GetException(ErrorCode.USER, User.USER_DUPLICATE, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode; 
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                    req.password = CryptoUtility.EncryptPassword(req.username, req.password, false);
                    z_user z_ = new z_user()
                    {
                        activated = true,
                        pwd = req.password,
                        username = req.username,
                        addr = req.addr,
                        company_id = Company.DEMO,
                        authen = new authen_key(),
                        cpw_type = 1,
                        createAt = 0,
                        create_date = DateTime.Now,
                        date_of_birth = null,
                        editAt = 0,
                        edit_date = DateTime.Now,
                        email = req.username,
                        f_name = req.f_name,
                        l_name = req.l_name,
                        id_card = "",
                        image = new img(),
                        inactive = false,
                        lc_acc = null,
                        lf_limit = 0,
                        lf_times = 0,
                        locked = false,
                        mobile = req.mobile,
                        push_key = req.push_key,
                        recv_news = true,
                        user_id = this.GetIncrement(typeof(z_user).Name),
                        user_type = req.user_type,
                        online = new status_online()
                    };

                    z_user.InsertOne(z_);

                    output_model.Data = new UserModels()
                    {
                        addr = z_.addr,
                        date_of_birth = z_.date_of_birth,
                        email = z_.email,
                        f_name = z_.f_name,
                        l_name = z_.l_name,
                        id_card = z_.id_card,
                        mobile = z_.mobile,
                        recv_news = z_.recv_news,
                        username = z_.username,
                        user_type = z_.user_type
                    };

                    output_model.ErrorCode = ErrorCode.SUCCESS;
                    output_model.ErrorSubCode = ErrorCode.SUCCESS;
                }
                else 
                 {// ข้อมูล input ไม่ถูกต้อง
                    OutputBaseModel err = this.GetException(ErrorCode.USER, User.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserLoginModelsOut Login(UserLoginModelsIn req, Int64 lang_id)
        {
            try
            {
                UserLoginModelsOut output_model = new UserLoginModelsOut();
                var z_user = this._mongodb.GetCollection<z_user>(typeof(z_user).Name);
                if (req != null) 
                {
                    req.password = CryptoUtility.EncryptPassword(req.username, req.password, false);
                    List<z_user> info_user = z_user.Find(o => o.username == req.username && o.pwd == req.password && o.inactive == false).ToList();
                    if (info_user.Count > 0)
                    {
                        z_user info = info_user.First();
                        if (info.activated != true)
                        { // ยังไม่ได้ยืนยันตัวตน
                            OutputBaseModel err = this.GetException(ErrorCode.USER, User.USER_NO_ACTIVATED, lang_id, "");
                            output_model.ErrorCode = err.ErrorCode;
                            output_model.ErrorSubCode = err.ErrorSubCode;
                            output_model.Title = err.Title;
                            output_model.ErrorMesg = err.ErrorMesg;
                            output_model.ErrorDetail = err.ErrorDetail;
                            return output_model;
                        }
                        else if (info.locked != false)
                        {// ถูก locked การใช้งาน
                            OutputBaseModel err = this.GetException(ErrorCode.USER, User.USER_LOCKED, lang_id, "");
                            output_model.ErrorCode = err.ErrorCode;
                            output_model.ErrorSubCode = err.ErrorSubCode;
                            output_model.Title = err.Title;
                            output_model.ErrorMesg = err.ErrorMesg;
                            output_model.ErrorDetail = err.ErrorDetail;
                            return output_model;
                        }
                       
                        output_model.Data = new UserLoginModels()
                        {
                            addr = info.addr,
                            authen = new authen_key(),
                            date_of_birth = info.date_of_birth,
                            email = info.email,
                            f_name = info.f_name,
                            l_name = info.l_name,
                            image = new img(),
                            mobile = info.mobile,
                            user_id = info.user_id,
                            user_type = info.user_type,
                            company_id = info.company_id
                        };

                        online(output_model.Data.user_id, output_model.Data.company_id);
                        string ch = ConfigurationManager.AppSettings["isLogLogin"].ToString();
                        if (ch == "true") 
                        {
                            isLogUserLoginLogout(output_model.Data.user_id, log_user_type.USER_ONLINE, output_model.Data.company_id);
                        }
                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else 
                    { // ไม่พบข้อมูบผู้ใช้งาน
                        OutputBaseModel err = this.GetException(ErrorCode.USER, User.NO_DATA_USER, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                { // ข้อมูล input ไม่ถูกต้อง
                    OutputBaseModel err = this.GetException(ErrorCode.USER, User.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }



        /// <summary>
        /// Logout
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="company_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel Logout(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                OutputBaseModel output_model = new OutputBaseModel();
                if (user_id > 0 && company_id > 0 && lang_id > 0)
                {
                    offline(user_id,company_id);
                    string ch = ConfigurationManager.AppSettings["isLogLogin"].ToString();
                    if (ch == "true")
                    {
                        isLogUserLoginLogout(user_id, log_user_type.USER_OFFLINE, company_id);
                    }
                }
                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }


        /// <summary>
        /// UserInfo
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserInfoModelsOut UserInfo(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                var z_user = this._mongodb.GetCollection<z_user>(typeof(z_user).Name);
                UserInfoModelsOut output_model = new UserInfoModelsOut();
                if (user_id > 0 && company_id > 0)
                {
                    List<z_user> info_user = z_user.Find(t => t.user_id == user_id && t.inactive == false && t.locked == false && t.activated == true && t.company_id == company_id).ToList();
                    if (info_user.Count > 0)
                    {
                        z_user info = info_user.First();
                        output_model.Data = new UserInfoModels()
                        {
                            addr = info.addr,
                            date_of_birth = info.date_of_birth,
                            email = info.email,
                            f_name = info.f_name,
                            id_card = info.id_card,
                            image = info.image,
                            l_name = info.l_name,
                            mobile = info.mobile,
                            username = info.username,
                            user_id = info.user_id,
                            user_type = info.user_type,
                            status_online = new status_online() { 
                                is_online = info.online.is_online,
                                offline_datetime = info.online.offline_datetime,
                                online_datetime = info.online.online_datetime
                            }
                        };

                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else 
                    { // หา user ไม่เจอ
                        OutputBaseModel err = this.GetException(ErrorCode.USER, User.NO_DATA_USER, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                { // ระบุบ user_id ไม่ถูก
                    OutputBaseModel err = this.GetException(ErrorCode.USER, User.INPUT_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

        /// <summary>
        /// UserInfo
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel Position(UserpositionModelsIn req, Int64 type, Int64 lang_id)
        {
            try
            {
                OutputBaseModel output_model = new OutputBaseModel();
                if (req != null) 
                {
                   isLogUserPosition(req.user_id, type, req.company_id, req.positions);
                }
                output_model.ErrorCode = ErrorCode.SUCCESS;
                output_model.ErrorSubCode = ErrorCode.SUCCESS;
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }
    }
}