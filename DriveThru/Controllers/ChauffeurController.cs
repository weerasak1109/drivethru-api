﻿using DriveThru.Management;
using DriveThru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DriveThru.Controllers
{
    public class ChauffeurController : ApiController
    {
        /// <summary>
        /// Chauffeur Register
        /// </summary>
        /// <param name="req">ข้อมูล</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("chauffeur/register/{lang_id}")]
        public OutputBaseModel PostChauffeurRegister(ChauffeurModelsIn req, Int64 lang_id)
        {
            try
            {
                return ChauffeurManagement.I.ChauffeurRegister(req, lang_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Chauffeur Info
        /// </summary>
        /// <param name="ch_id">รหัสคนขับ</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("chauffeur/info/{ch_id}/{company_id}/{lang_id}")]
        public ChauffeurInfoModelsOut GetChauffeurInfo(Int64 ch_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return ChauffeurManagement.I.ChauffeurInfo(ch_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Chauffeur List
        /// </summary>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("chauffeur/list/{company_id}/{lang_id}")]
        public ChauffeurListModelsOut GetChauffeurList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                return ChauffeurManagement.I.ChauffeurList(company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Chauffeur
        /// </summary>
        /// <param name="ch_id">รหัสคนขับ</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("chauffeur/delete/{ch_id}/{lang_id}")]
        public OutputBaseModel DeleteChauffeur(Int64 ch_id, Int64 lang_id)
        {
            try
            {
                return ChauffeurManagement.I.DeleteChauffeur(ch_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}