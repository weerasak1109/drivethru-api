﻿using DriveThru.Management;
using DriveThru.Models;
using DriveThru.Utilitys;
using System;
using System.Web.Http;


namespace DriveThru.Controllers
{
    public class UserController : ApiController
    {
        /// <summary>
        /// Register
        /// </summary>
        /// <param name="req">ข้อมูล</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("user/register/{lang_id}")]
        public UserModelsOut PostRegister(UserModelsIn req, Int64 lang_id)
        {
            try
            {
                return UserManagement.I.Register(req, lang_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="req">ข้อมูล</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("user/login/{lang_id}")]
        public UserLoginModelsOut PostLogin(UserLoginModelsIn req, Int64 lang_id)
        {
            try
            {
                return UserManagement.I.Login(req, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="company_id">รหัสบริษัท</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("user/logout/{user_id}/{company_id}/{lang_id}")]
        public OutputBaseModel PutLogout(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return UserManagement.I.Logout(user_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// user info
        /// </summary>
        /// <param name="user_id">รหัสผู้ใช้</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("user/info/{user_id}/{company_id}/{lang_id}")]
        public UserInfoModelsOut GetUserInfo(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return UserManagement.I.UserInfo(user_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// User Position
        /// </summary>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("user/position/{lang_id}")]
        public OutputBaseModel PostUserPosition(UserpositionModelsIn req, Int64 lang_id)
        {
            try
            {
                return UserManagement.I.Position(req, log_user_type.USER_POSITION, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Chauffeur Position
        /// </summary>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("chauffeur/position/{lang_id}")]
        public OutputBaseModel PostChauffeurPosition(UserpositionModelsIn req, Int64 lang_id)
        {
            try
            {
                return UserManagement.I.Position(req, log_user_type.CHAUFFEUE_POSITION, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}