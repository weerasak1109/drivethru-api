﻿using DriveThru.Management;
using DriveThru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static DriveThru.Models.MongoDataBase.CarModels;

namespace DriveThru.Controllers
{
    public class CarController : ApiController
    {
        /// <summary>
        /// Car Register
        /// </summary>
        /// <param name="req">ข้อมูล</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("car/register/{lang_id}")]
        public OutputBaseModel PostCarRegister(CarModelsIn req, Int64 lang_id)
        {
            try
            {
                return CarManagement.I.CarRegister(req, lang_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Car Info
        /// </summary>
        /// <param name="car_id">รหัสรถ</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("car/info/{car_id}/{company_id}/{lang_id}")]
        public CarInfoModelsOut GetCarInfo(Int64 car_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return CarManagement.I.CarInfo(car_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Car List
        /// </summary>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("car/list/{company_id}/{lang_id}")]
        public CarListModelsOut GetCarList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                return CarManagement.I.CarList( company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete List
        /// </summary>
        /// <param name="car_id">รหัสรถ</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("car/delete/{car_id}/{company_id}/{lang_id}")]
        public OutputBaseModel DeleteCar(Int64 car_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return CarManagement.I.DeleteCar(car_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}