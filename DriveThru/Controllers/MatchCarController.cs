﻿using DriveThru.Management;
using DriveThru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DriveThru.Controllers
{
    public class MatchCarController : ApiController
    {
        /// <summary>
        ///  Match คนขับ กับ รถ
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [Route("match/approved/{lang_id}")]
        public OutputBaseModel PostMatchChauffeurCar(MatchChauffeurCarModelIn req, Int64 lang_id)
        {
            try
            {
                return MatchCarManagement.I.MatchChauffeurCar(req,lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// ข้อมูล คนขับ กับ รถ info
        /// </summary>
        /// <param name="car_id">รหัสรถ</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("match/Chauffeurcar/info/{app_car_id}/{company_id}/{lang_id}")]
        public ChauffeurCarModelInfoOut GetChauffeurCarinfo(Int64 app_car_id,Int64 company_id, Int64 lang_id)
        {
            try
            {
                return MatchCarManagement.I.ChauffeurCarinfo(app_car_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// ข้อมูล คนขับ กับ รถ list
        /// </summary>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("match/Chauffeurcar/list/{company_id}/{lang_id}")]
        public ChauffeurCarModelListOut GetChauffeurCarList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                return MatchCarManagement.I.ChauffeurCarList(company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}