﻿using DriveThru.Management;
using DriveThru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DriveThru.Controllers
{
    public class JobController : ApiController
    {
        /// <summary>
        /// สร้างงาน
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [Route("job/create/{lang_id}")]
        public OutputBaseModel PostCreateJob(JobModelsIn req ,Int64 lang_id)
        {
            try
            {
                return JobManagement.I.CreateJob(req,lang_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// แสดงข้อมูลงาน list
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [Route("job/get/list/{company_id}/{lang_id}")]
        public JobModelsOutList GetJobList(Int64 company_id, Int64 lang_id)
        {
            try
            {
                return JobManagement.I.JobList(company_id, lang_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// แสดงข้อมูลงาน info
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [Route("job/get/info/{job_id}/{company_id}/{lang_id}")]
        public JobModelsOutInfo GetJobInfo(Int64 job_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return JobManagement.I.JobInfo(job_id, company_id, lang_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[Route("job/close/{lang_id}")]
        //public OutputBaseModel DeleteCloseJob(Int64 lang_id)
        //{
        //    try
        //    {
        //        OutputBaseModel model = new OutputBaseModel();
        //        return model;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[Route("job/approved/{lang_id}")]
        //public OutputBaseModel PostApprovedJob(Int64 lang_id)
        //{
        //    try
        //    {
        //        OutputBaseModel model = new OutputBaseModel();
        //        return model;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}