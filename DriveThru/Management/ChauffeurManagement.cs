﻿using DriveThru.DBConnector;
using DriveThru.Models;
using DriveThru.Services;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Management
{
    public class ChauffeurManagement
    {
        #region Singleton
        private ChauffeurService _service;
        private UserService _u_service;
        private static readonly Lazy<ChauffeurManagement> instance = new Lazy<ChauffeurManagement>(() => Activator.CreateInstance<ChauffeurManagement>());
        public static ChauffeurManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public ChauffeurManagement()
        {
        }
        #endregion

        /// <summary>
        /// Chauffeur Register
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel ChauffeurRegister(ChauffeurModelsIn req, Int64 lang_id)
        {
            OutputBaseModel res = null;
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;

                _u_service = new UserService(conn);
                UserModelsIn user = new UserModelsIn() 
                {
                    addr = req.addr,
                    date_of_birth = req.date_of_birth,
                    email = req.email,
                    f_name = req.f_name,
                    id_card = req.id_card,
                    l_name = req.l_name,
                    mobile = req.mobile,
                    password = req.password,
                    push_key = "",
                    recv_news = false,
                    username = req.username,
                    user_type = req.user_type
                };
                UserModelsOut Reg_user = _u_service.Register(user, lang_id);
                if (Reg_user.ErrorCode == 1)
                {
                    _service = new ChauffeurService(conn);
                    res = _service.ChauffeurRegister(req, lang_id);
                }
                else 
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = Reg_user.ErrorCode,
                        ErrorDetail = Reg_user.ErrorDetail,
                        ErrorMesg = Reg_user.ErrorMesg,
                        ErrorSubCode = Reg_user.ErrorSubCode,
                        FlowControl = Reg_user.FlowControl,
                        Title = Reg_user.Title
                    };
                }


            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CHAUFFEUR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// Chauffeur Info
        /// </summary>
        /// <param name="car_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurInfoModelsOut ChauffeurInfo(Int64 ch_id, Int64 company_id, Int64 lang_id)
        {
            ChauffeurInfoModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new ChauffeurService(conn);
                res = _service.ChauffeurInfo(ch_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CHAUFFEUR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new ChauffeurInfoModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new ChauffeurInfoModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

        /// <summary>
        /// Chauffeur List
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurListModelsOut ChauffeurList(Int64 company_id, Int64 lang_id)
        {
            ChauffeurListModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new ChauffeurService(conn);
                res = _service.ChauffeurList(company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CHAUFFEUR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new ChauffeurListModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new ChauffeurListModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

        /// <summary>
        /// Delete Chauffeur
        /// </summary>
        /// <param name="ch_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel DeleteChauffeur(Int64 ch_id, Int64 lang_id)
        {
            OutputBaseModel res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new ChauffeurService(conn);
                res = _service.DeleteChauffeur(ch_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CHAUFFEUR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }
    }
}