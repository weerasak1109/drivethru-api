﻿using DriveThru.DBConnector;
using DriveThru.Models;
using DriveThru.Services;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Management
{
    public class MatchCarManagement
    {
        #region Singleton
        private MatchCarService _service;
        private static readonly Lazy<MatchCarManagement> instance = new Lazy<MatchCarManagement>(() => Activator.CreateInstance<MatchCarManagement>());
        public static MatchCarManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public MatchCarManagement()
        {
        }
        #endregion

        /// <summary>
        /// Match คนขับ กับ รถ
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel MatchChauffeurCar(MatchChauffeurCarModelIn req,Int64 lang_id)
        {
            OutputBaseModel res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new MatchCarService(conn);
                res = _service.MatchChauffeurCar(req,lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// ข้อมูล คนขับ กับ รถ info
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurCarModelInfoOut ChauffeurCarinfo(Int64 app_car_id, Int64 company_id, Int64 lang_id)
        {
            ChauffeurCarModelInfoOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new MatchCarService(conn);
                res = _service.ChauffeurCarinfo(app_car_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new ChauffeurCarModelInfoOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new ChauffeurCarModelInfoOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// ข้อมูล คนขับ กับ รถ list
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public ChauffeurCarModelListOut ChauffeurCarList(Int64 company_id, Int64 lang_id)
        {
            ChauffeurCarModelListOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new MatchCarService(conn);
                res = _service.ChauffeurCarList(company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.MATCH_CHAUFFEUR_CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new ChauffeurCarModelListOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new ChauffeurCarModelListOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }
    }
}