﻿using DriveThru.DBConnector;
using DriveThru.Models;
using DriveThru.Services;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;

namespace DriveThru.Management
{
    public class UserManagement
    {
        #region Singleton
        private UserService _service;
        private static readonly Lazy<UserManagement> instance = new Lazy<UserManagement>(() => Activator.CreateInstance<UserManagement>());
        public static UserManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public UserManagement()
        {
        }
        #endregion

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserModelsOut Register(UserModelsIn req, Int64 lang_id)
        {
            UserModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new UserService(conn);
                res = _service.Register(req, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.USER, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new UserModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new UserModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserLoginModelsOut Login(UserLoginModelsIn req, Int64 lang_id)
        {
            UserLoginModelsOut res = null;
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new UserService(conn);
                res = _service.Login(req, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.USER, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new UserLoginModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new UserLoginModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="company_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel Logout(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            OutputBaseModel res = null;
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new UserService(conn);
                res = _service.Logout(user_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.USER, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

        /// <summary>
        /// UserInfo
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserInfoModelsOut UserInfo(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            UserInfoModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new UserService(conn);
                res = _service.UserInfo(user_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.USER, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new UserInfoModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new UserInfoModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }


        /// <summary>
        /// UserInfo
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel Position(UserpositionModelsIn req,Int64 type, Int64 lang_id)
        {
            OutputBaseModel res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new UserService(conn);
                res = _service.Position( req, type, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.USER, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

    }
}