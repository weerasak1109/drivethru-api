﻿using DriveThru.DBConnector;
using DriveThru.Models;
using DriveThru.Services;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static DriveThru.Models.MongoDataBase.CarModels;

namespace DriveThru.Management
{
    public class CarManagement
    {
        #region Singleton
        private CarService _service;
        private static readonly Lazy<CarManagement> instance = new Lazy<CarManagement>(() => Activator.CreateInstance<CarManagement>());
        public static CarManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public CarManagement()
        {
        }
        #endregion

        /// <summary>
        /// Car Register
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel CarRegister(CarModelsIn req, Int64 lang_id)
        {
            OutputBaseModel res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CarService(conn);
                res = _service.CarRegister(req, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// Car Info
        /// </summary>
        /// <param name="car_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public CarInfoModelsOut CarInfo(Int64 car_id, Int64 company_id, Int64 lang_id)
        {
            CarInfoModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CarService(conn);
                res = _service.CarInfo(car_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new CarInfoModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new CarInfoModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

        /// <summary>
        /// Car List
        /// </summary>
        /// <param name="car_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public CarListModelsOut CarList(Int64 company_id, Int64 lang_id)
        {
            CarListModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CarService(conn);
                res = _service.CarList( company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new CarListModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new CarListModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

        /// <summary>
        /// Delete Car
        /// </summary>
        /// <param name="car_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel DeleteCar(Int64 car_id, Int64 company_id, Int64 lang_id)
        {
            OutputBaseModel res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CarService(conn);
                res = _service.DeleteCar(car_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.CAR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

    }
}