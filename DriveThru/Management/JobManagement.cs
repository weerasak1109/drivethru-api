﻿using DriveThru.DBConnector;
using DriveThru.Models;
using DriveThru.Services;
using DriveThru.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Management
{
    public class JobManagement
    {
        #region Singleton
        private JobService _service;
        private static readonly Lazy<JobManagement> instance = new Lazy<JobManagement>(() => Activator.CreateInstance<JobManagement>());
        public static JobManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public JobManagement()
        {
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public OutputBaseModel CreateJob(JobModelsIn req,Int64 lang_id)
        {
            OutputBaseModel res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new JobService(conn);
                res = _service.CreateJob(req, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.JOB, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new OutputBaseModel()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new OutputBaseModel()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public JobModelsOutList JobList(Int64 company_id, Int64 lang_id)
        {
            JobModelsOutList res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new JobService(conn);
                res = _service.JobList(company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.JOB, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new JobModelsOutList()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new JobModelsOutList()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="job_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public JobModelsOutInfo JobInfo(Int64 job_id, Int64 company_id, Int64 lang_id)
        {
            JobModelsOutInfo res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new JobService(conn);
                res = _service.JobInfo(job_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.JOB, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new JobModelsOutInfo()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new JobModelsOutInfo()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }

            return res;
        }
    }
}