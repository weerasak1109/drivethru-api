﻿using MongoDB.Driver;
using System;
using System.Configuration;

namespace DriveThru.DBConnector
{
    public class MongoConnector
    {
        private IMongoDatabase db;

        private string MongoDBConectionString;


        #region Singleton
        private static MongoConnector _instance = null;

        public static MongoConnector I()
        {
            if (_instance == null)
                _instance = new MongoConnector();
            return _instance;
        }

        private MongoConnector()
        {
            try
            {
                MongoDBConectionString = ConfigurationManager.ConnectionStrings["MongoDBConectionString"].ConnectionString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DBConnector
        public IMongoDatabase DBConnect
        {
            get
            {
                var client = new MongoClient(MongoDBConectionString);
                 db = client.GetDatabase(new MongoUrl(MongoDBConectionString).DatabaseName);
                return db;

            }
        }

        #endregion
    }
}