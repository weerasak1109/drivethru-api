﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Models.MongoDataBase
{
    public class CarModels
    {
        //input
        /// <summary>
        /// ข้อมูลสมัครของรถ
        /// </summary>
        public class CarModelsIn
        {
            /// <summary>
            /// ยี่ห้อรถ
            /// </summary>
            public string car_brand { get; set; }
            /// <summary>
            /// รุ่นรถ
            /// </summary>
            public string car_model { get; set; }
            /// <summary>
            /// สีรถ
            /// </summary>
            public string car_color { get; set; }
            /// <summary>
            /// ทะเบียนรถ
            /// </summary>
            public string license_plate { get; set; }
            /// <summary>
            /// ผู้สร้าง
            /// </summary>
            public Int64 createAt { get; set; }
            /// <summary>
            /// รหัสบริษัท
            /// </summary>
            public Int64 company_id { get; set; }

        }


        // output
        public class CarInfoModelsOut : OutputBaseModel
        {
            public CarInfoModels Data { get; set; }
        }

        public class CarListModelsOut : OutputBaseModel
        {
            public List<CarInfoModels> Data { get; set; }
        }


        public class CarInfoModels : DataMasterEntity
        {
            /// <summary>
            /// รหัสผรถ
            /// </summary>
            public Int64 car_id { get; set; }
            /// <summary>
            /// ยี่ห้อรถ
            /// </summary>
            public string car_brand { get; set; }
            /// <summary>
            /// รุ่นรถ
            /// </summary>
            public string car_model { get; set; }
            /// <summary>
            /// สีรถ
            /// </summary>
            public string car_color { get; set; }
            /// <summary>
            /// ทะเบียนรถ
            /// </summary>
            public string license_plate { get; set; }
            /// <summary>
            /// ล็อกการใช้งาน
            /// </summary>
            public bool locked { get; set; }
        }
    }
}