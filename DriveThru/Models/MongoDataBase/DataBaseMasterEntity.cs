﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Models.MongoDataBase
{
    public class DataBaseMasterEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public class zz_log_user_type : DataMasterEntity
        {
            /// <summary>
            /// 
            /// </summary>
            public long log_type_user_id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string log_type_user_name { get; set; }
        }


        /// <summary>
        /// authen 
        /// </summary>
        public class authen_key
        {
            /// <summary>
            /// token
            /// </summary>
            public string key { get; set; }
        }

        /// <summary>
        /// img
        /// </summary>
        public class img
        {
            /// <summary>
            /// รหัสรูปออนไลน์
            /// </summary>
            public Int64 img_id { get; set; }
            /// <summary>
            /// ที่อยู่รูปออนไลน์
            /// </summary>
            public string img_path { get; set; }

        }
        /// <summary>
        /// สถานะการใช้งาน
        /// </summary>
        public class status_online
        {
            public bool is_online { get; set; }
            public DateTime online_datetime { get; set; }
            public DateTime? offline_datetime { get; set; }
        }



        /// <summary>
        /// ข้อมูลตำแหน่ง
        /// </summary>
        public class positions
        {
            /// <summary>
            /// 
            /// </summary>
            public double latitude { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double longitude { get; set; }
            /// <summary>
            /// ชื่อสถานที่ตรงนั้น
            /// </summary>
            public string details { get; set; }
        }

        /// <summary>
        /// ข้อมูลรถในงาน
        /// </summary>
        public class carslist
        {
            /// <summary>
            /// รหัส
            /// </summary>
            public long app_car_id { get; set; }
            /// <summary>
            /// รหัสผรถ
            /// </summary>
            public Int64 car_id { get; set; }
            /// <summary>
            /// ยี่ห้อรถ
            /// </summary>
            public string car_brand { get; set; }
            /// <summary>
            /// รุ่นรถ
            /// </summary>
            public string car_model { get; set; }
            /// <summary>
            /// สีรถ
            /// </summary>
            public string car_color { get; set; }
            /// <summary>
            /// ทะเบียนรถ
            /// </summary>
            public string license_plate { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long ch_id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ch_name { get; set; }
        }

        /// <summary>
        /// z_user ผู้ใช้
        /// </summary>
        [BsonIgnoreExtraElements]
        public class z_user : DataMasterEntity
        {
            /// <summary>
            /// รหัสผู้ใช้
            /// </summary>
            public Int64 user_id { get; set; }
            /// <summary>
            /// ประเภทผู้ใช้
            /// </summary>
            public Int64 user_type { get; set; }
            /// <summary>
            /// ยูสเซ่อเนม
            /// </summary>
            public string username { get; set; }
            /// <summary>
            /// รหัสผ่าน
            /// </summary>
            public string pwd { get; set; }
            /// <summary>
            /// วันที่บัญชีจะหมดอายุ
            /// </summary>
            public DateTime? lc_acc { get; set; }
            /// <summary>
            /// จำนวนครั้งที่เข้าสู่ระบบผิดพลาด
            /// </summary>
            public Int64 lf_times { get; set; }
            /// <summary>
            /// จำนวนครั้งสูงสุดที่เข้าสู่ระบบผิดพลาด
            /// </summary>
            public Int64 lf_limit { get; set; }
            /// <summary>
            /// วิธีการแจ้งให้เปลี่ยนรหัสผ่าน
            /// </summary>
            public Int64 cpw_type { get; set; }
            /// <summary>
            /// ชื่อ
            /// </summary>
            public string f_name { get; set; }
            /// <summary>
            /// นามสกุล
            /// </summary>
            public string l_name { get; set; }
            /// <summary>
            /// เบอร์มือถือ
            /// </summary>
            public string mobile { get; set; }
            /// <summary>
            /// อีเมล
            /// </summary>
            public string email { get; set; }
            /// <summary>
            /// เลขประจำตัวประชาชน
            /// </summary>
            public string id_card { get; set; }
            /// <summary>
            /// วันเกิด
            /// </summary>
            public DateTime? date_of_birth { get; set; }
            /// <summary>
            /// บ้านเลขที่ หมู่ หมู่บ้าน
            /// </summary>
            public string addr { get; set; }
            /// <summary>
            /// รับข่าวสาร
            /// </summary>
            public bool recv_news { get; set; }
            /// <summary>
            /// ล๊อกการใช้งาน
            /// </summary>
            public bool locked { get; set; }
            /// <summary>
            /// ยืนยันผู้ใช้
            /// </summary>
            public bool activated { get; set; }
            /// <summary>
            /// รหัสแจ้งเตือน
            /// </summary>
            public string push_key { get; set; }
            /// <summary>
            /// authen เดิม
            /// </summary>
            public authen_key authen { get; set; }
            /// <summary>
            /// รูปผู้ใช้งาน
            /// </summary>
            public img image { get; set; }
            /// <summary>
            /// สถานะ online / offline
            /// </summary>
            public status_online online { get; set; }
        }

        /// <summary>
        ///  ข้อมูลรถ
        /// </summary>
        [BsonIgnoreExtraElements]
        public class m_car : DataMasterEntity
        {
            /// <summary>
            /// รหัสผรถ
            /// </summary>
            public Int64 car_id { get; set; }
            /// <summary>
            /// ยี่ห้อรถ
            /// </summary>
            public string car_brand { get; set; }
            /// <summary>
            /// รุ่นรถ
            /// </summary>
            public string car_model { get; set; }
            /// <summary>
            /// สีรถ
            /// </summary>
            public string car_color { get; set; }
            /// <summary>
            /// ทะเบียนรถ
            /// </summary>
            public string license_plate { get; set; }
            /// <summary>
            /// ล็อกการใช้งาน
            /// </summary>
            public bool locked { get; set; }
        }


        /// <summary>
        /// zz_user_type  ผู้ใช้
        /// </summary>
        [BsonIgnoreExtraElements]
        public class zz_user_type : DataMasterEntity
        {
            /// <summary>
            /// 1 =Admin /2 = คนขับ / 3 =ผู้โดยสาร
            /// </summary>
            public long user_type { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string user_type_name { get; set; }
            /// <summary>
            /// ข้อสังเกต
            /// </summary>
            public string remark { get; set; }
        }

        /// <summary>
        /// m_chauffeur  ผู้ขับรถ
        /// </summary>
        [BsonIgnoreExtraElements]
        public class m_chauffeur : DataMasterEntity
        {
            /// <summary>
            /// 
            /// </summary>
            public long ch_id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ch_name { get; set; }
            /// <summary>
            /// ชื่อ
            /// </summary>
            public string f_name { get; set; }
            /// <summary>
            /// นามสกุล
            /// </summary>
            public string l_name { get; set; }
            /// <summary>
            /// เบอร์มือถือ
            /// </summary>
            public string mobile { get; set; }
            /// <summary>
            /// อีเมล
            /// </summary>
            public string email { get; set; }
            /// <summary>
            /// เลขประจำตัวประชาชน
            /// </summary>
            public string id_card { get; set; }
            /// <summary>
            /// วันเกิด
            /// </summary>
            public DateTime? date_of_birth { get; set; }
            /// <summary>
            /// บ้านเลขที่ หมู่ หมู่บ้าน
            /// </summary>
            public string addr { get; set; }
            /// <summary>
            /// ล็อกการใช้งาน
            /// </summary>
            public bool locked { get; set; }
        }

        /// <summary>
        /// จับคู่ รถ กับ คนขับ
        /// </summary>
        [BsonIgnoreExtraElements]
        public class m_approved_car : DataMasterEntity
        {
            /// <summary>
            /// รหัส
            /// </summary>
            public long app_car_id { get; set; }
            /// <summary>
            /// รหัสผรถ
            /// </summary>
            public Int64 car_id { get; set; }
            /// <summary>
            /// ยี่ห้อรถ
            /// </summary>
            public string car_brand { get; set; }
            /// <summary>
            /// รุ่นรถ
            /// </summary>
            public string car_model { get; set; }
            /// <summary>
            /// สีรถ
            /// </summary>
            public string car_color { get; set; }
            /// <summary>
            /// ทะเบียนรถ
            /// </summary>
            public string license_plate { get; set; }
            /// <summary>
            /// สถานะการอนุมัติ
            /// </summary>
            public bool approved { get; set; }
            /// <summary>
            /// ใครอนุมัติ
            /// </summary>
            public long approvedAt { get; set; }
            /// <summary>
            /// วันที่ อนุมัติ
            /// </summary>
            public DateTime? approved_date { get; set; }
            /// <summary>
            /// ข้อมูลคนขับ
            /// </summary>
            public List<chauffeurs> chauffeurs { get; set; }
        }

        /// <summary>
        /// สร้างงาน
        /// </summary>
        public class m_job : DataMasterEntity
        {
            /// <summary>
            /// รหัสผู้ใช้
            /// </summary>
            public Int64 user_id { get; set; }
            /// <summary>
            /// รหัสงาน
            /// </summary>
            public Int64 job_id { get; set; }
            /// <summary>
            /// ชื่องาน
            /// </summary>
            public string job_name { get; set; }
            /// <summary>
            /// รายละเอียดงาน
            /// </summary>
            public string job_detail { get; set; }
            /// <summary>
            /// ต้นทาง
            /// </summary>
            public string s_terminal { get; set; }
            /// <summary>
            /// ปลายทาง
            /// </summary>
            public string e_terminal { get; set; }
            /// <summary>
            /// ราคาตลอดสาย
            /// </summary>
            public double ticket_price { get; set; }
            /// <summary>
            /// สถานะการอนุมัติ
            /// </summary>
            public bool approved { get; set; }
            /// <summary>
            /// ใครอนุมัติ
            /// </summary>
            public long approvedAt { get; set; }
            /// <summary>
            /// วันที่ อนุมัติ
            /// </summary>
            public DateTime? approved_date { get; set; }
            /// <summary>
            /// เส้นทางการเดินรถ
            /// </summary>
            public List<positions> positions { get; set; }
            /// <summary>
            /// ข้อมูลรถในงาน
            /// </summary>
            public List<carslist> carslists { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class t_log_user : DataMasterEntity
        {
            /// <summary>
            /// 
            /// </summary>
            public Int64 log_id { get; set; }
            /// <summary>
            /// รหัส
            /// </summary>
            public Int64 log_type_user_id { get; set; }
            /// <summary>
            /// ชื่อ log
            /// </summary>
            public string log_type_user_name { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class t_log_position : DataMasterEntity
        {
            /// <summary>
            /// 
            /// </summary>
            public Int64 log_id { get; set; }
            /// <summary>
            /// รหัส
            /// </summary>
            public Int64 log_type_user_id { get; set; }
            /// <summary>
            /// ชื่อ log
            /// </summary>
            public string log_type_user_name { get; set; }
            /// <summary>
            /// ตำแหน่ง
            /// </summary>
            public positions positions { get; set; }
        }



        /// <summary>
        /// 
        /// </summary>
        public class zz_log_approved_type : DataMasterEntity
        {
            /// <summary>
            /// 
            /// </summary>
            public long log_type_approved_id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string log_type_approved_name { get; set; }

        }
    }
}

