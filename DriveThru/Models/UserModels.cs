﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;

namespace DriveThru.Models
{
    //Input
    public class UserModelsIn
    {
        /// <summary>
        /// ประเภทผู้ใช้  1 =Admin /2 = คนขับ / 3 =ผู้โดยสาร
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ยูสเซ่อเนม
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// รหัสผ่าน
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// เลขประจำตัวประชาชน
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }
        /// <summary>
        /// รับข่าวสาร
        /// </summary>
        public bool recv_news { get; set; }
        /// <summary>
        /// รหัสแจ้งเตือน
        /// </summary>
        public string push_key { get; set; }

    }

    public class UserLoginModelsIn 
    {
        /// <summary>
        /// ประเภทผู้ใช้  1 =Admin /2 = คนขับ / 3 =ผู้โดยสาร
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ยูสเซ่อเนม
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// รหัสผ่าน
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class UserpositionModelsIn
    {
        /// <summary>
        /// รหัสผู้ใช้
        /// </summary>
        public Int64 user_id { get; set; }
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public positions positions { get; set; }

    }



    /// output
    public class UserModelsOut : OutputBaseModel
    {
        public UserModels Data { get; set; }
    }

    public class UserLoginModelsOut : OutputBaseModel
    {
        public UserLoginModels Data { get; set; }
    }

    public class UserInfoModelsOut : OutputBaseModel
    {
        public UserInfoModels Data { get; set; }
    }

    // 
    public class UserModels 
    {
        /// <summary>
        /// ประเภทผู้ใช้  1 =Admin /2 = คนขับ / 3 =ผู้โดยสาร
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ยูสเซ่อเนม
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// เลขประจำตัวประชาชน
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }
        /// <summary>
        /// รับข่าวสาร
        /// </summary>
        public bool recv_news { get; set; }
    }

    public class UserLoginModels 
    {
        /// <summary>
        /// รหัสผู้ใช้
        /// </summary>
        public Int64 user_id { get; set; }
        /// <summary>
        /// ประเภทผู้ใช้
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }
        /// <summary>
        /// authen เดิม
        /// </summary>
        public authen_key authen { get; set; }
        /// <summary>
        /// รูปผู้ใช้งาน
        /// </summary>
        public img image { get; set; }
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
    }

    public class UserInfoModels 
    {
        /// <summary>
        /// รหัสผู้ใช้
        /// </summary>
        public Int64 user_id { get; set; }
        /// <summary>
        /// ประเภทผู้ใช้
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ยูสเซ่อเนม
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// เลขประจำตัวประชาชน
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }
        /// <summary>
        /// รูปผู้ใช้งาน
        /// </summary>
        public img image { get; set; }
        /// <summary>
        /// สถานะการใช้งาน
        /// </summary>
        public status_online status_online { get; set; }
    }
}