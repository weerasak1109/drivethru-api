﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Models
{
    /// <summary>
    /// in
    /// </summary>
    public class MatchChauffeurCarModelIn  
    {
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
        /// <summary>
        /// รหัสผรถ
        /// </summary>
        public Int64 car_id { get; set; }
        /// <summary>
        /// ผู้สร้าง
        /// </summary>
        public long user_add { get; set; }
        /// <summary>
        /// ข้อมูลคนขับ
        /// </summary>
        public List<chauffeurs> chauffeurs { get; set; }
    }


    /// <summary>
    /// output
    /// </summary>
    public class ChauffeurCarModelInfoOut : OutputBaseModel
    {
        public ChauffeurCarModelInfo Data { get; set; }
    }

    public class ChauffeurCarModelListOut : OutputBaseModel
    {
        public List<ChauffeurCarModelInfo> Data { get; set; }
    }


    public class ChauffeurCarModelInfo
    {
        /// <summary>
        /// รหัส
        /// </summary>
        public long app_car_id { get; set; }
        /// <summary>
        /// รหัสผรถ
        /// </summary>
        public Int64 car_id { get; set; }
        /// <summary>
        /// ยี่ห้อรถ
        /// </summary>
        public string car_brand { get; set; }
        /// <summary>
        /// รุ่นรถ
        /// </summary>
        public string car_model { get; set; }
        /// <summary>
        /// สีรถ
        /// </summary>
        public string car_color { get; set; }
        /// <summary>
        /// ทะเบียนรถ
        /// </summary>
        public string license_plate { get; set; }
        /// <summary>
        /// สถานะการอนุมัติ
        /// </summary>
        public bool approved { get; set; }
        /// <summary>
        /// ใครอนุมัติ
        /// </summary>
        public long approvedAt { get; set; }
        /// <summary>
        /// วันที่ อนุมัติ
        /// </summary>
        public DateTime? approved_date { get; set; }
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
        /// <summary>
        /// ข้อมูลคนขับ
        /// </summary>
        public List<chauffeurs> chauffeurs { get; set; }
    }

}