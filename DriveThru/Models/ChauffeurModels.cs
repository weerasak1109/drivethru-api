﻿using DriveThru.Models.MongoDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Models
{
    //input
    /// <summary>
    /// ข้อมูลสมัครของคนขับรถ
    /// </summary>
    public class ChauffeurModelsIn
    {
        /// <summary>
        /// ประเภทผู้ใช้  1 =Admin /2 = คนขับ / 3 =ผู้โดยสาร
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ยูสเซ่อเนม
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// รหัสผ่าน
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// เลขประจำตัวประชาชน
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }

        /// <summary>
        /// ผู้สร้าง
        /// </summary>
        public long user_add { get; set; }

    }


    // output
    public class ChauffeurInfoModelsOut : OutputBaseModel
    {
        public ChauffeurInfoModels Data { get; set; }
    }

    public class ChauffeurListModelsOut : OutputBaseModel
    {
        public List<ChauffeurInfoModels> Data { get; set; }
    }


    public class ChauffeurInfoModels : DataMasterEntity
    {
        /// <summary>
        /// รหัสผู้ขับ
        /// </summary>
        public long ch_id { get; set; }
        /// <summary>
        /// ชื่อเต็ม
        /// </summary>
        public string ch_name { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// เลขประจำตัวประชาชน
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }
    }
}