﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static DriveThru.Models.MongoDataBase.DataBaseMasterEntity;

namespace DriveThru.Models
{
    /// <summary>
    /// สร้างงาน
    /// </summary>
    public class JobModelsIn
    {
        /// <summary>
        /// รหัสผู้สร้างงาน
        /// </summary>
        public Int64 user_id { get; set; }
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
        /// ชื่องาน
        /// </summary>
        public string job_name { get; set; }
        /// <summary>
        /// รายละเอียดงาน
        /// </summary>
        public string job_detail { get; set; }
        /// <summary>
        /// ต้นทาง
        /// </summary>
        public string s_terminal { get; set; }
        /// <summary>
        /// ปลายทาง
        /// </summary>
        public string e_terminal { get; set; }
        /// <summary>
        /// ราคาตลอดสาย
        /// </summary>
        public double ticket_price { get; set; }
        /// เส้นทางการเดินรถ
        /// </summary>
        public List<positions> positions { get; set; }
        /// <summary>
        /// ข้อมูลรถในงาน
        /// </summary>
        public List<carslist> carslists { get; set; }
    }


    /// <summary>
    /// out
    /// </summary>
    public class JobModelsOutInfo : OutputBaseModel
    {
        public JobModelsOut Data { get; set; }

    }

    public class JobModelsOutList : OutputBaseModel
    {
        public List<JobModelsOut> Data { get; set; }
    }


    public class JobModelsOut
    {
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
        /// <summary>
        /// รหัสงาน
        /// </summary>
        public Int64 job_id { get; set; }
        /// ชื่องาน
        /// </summary>
        public string job_name { get; set; }
        /// <summary>
        /// รายละเอียดงาน
        /// </summary>
        public string job_detail { get; set; }
        /// <summary>
        /// ต้นทาง
        /// </summary>
        public string s_terminal { get; set; }
        /// <summary>
        /// ปลายทาง
        /// </summary>
        public string e_terminal { get; set; }
        /// <summary>
        /// ราคาตลอดสาย
        /// </summary>
        public double ticket_price { get; set; }
        /// <summary>
        /// เส้นทางการเดินรถ
        /// </summary>
        public List<positions> positions { get; set; }
        /// <summary>
        /// ข้อมูลรถในงาน
        /// </summary>
        public List<carslist> carslists { get; set; }
    }
}