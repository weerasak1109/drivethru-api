﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Models
{
    /// <summary>
    /// ข้อมูลคนขับ (m_approved_car)
    /// </summary>
    public class chauffeurs
    {
        /// <summary>
        /// รหัสผู้ขับ
        /// </summary>
        public long ch_id { get; set; }
        /// <summary>
        /// ชื่อเต็ม
        /// </summary>
        public string ch_name { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
    }
}