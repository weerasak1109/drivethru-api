﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveThru.Utilitys
{
    public class Language
    {
        public const int EN = 1;
        public const int TH = 2;
        public const int OTHER = 3;
    }

    public class Chauffeur
    {
        /// <summary>
        /// ผู้ขับรถซ็ำ
        /// </summary>
        public const int CHAUFFEUR_DUPLICATE = -1;
        /// <summary>
        /// ข้อมูล input ไม่ถูกต้อง
        /// </summary>
        public const int INPUT_ERROR = -2;

        public const int CHAUFFEUR_LOCKED = -3;

        public const int NO_DATA_CHAUFFEUR = -4;
    }

    public class Car
    {
        /// <summary>
        /// ไม่พบข้อมูล CAR
        /// </summary>
        public const int NO_DATA_CAR = -1;
        /// <summary>
        /// ข้อมูล input ไม่ถูกต้อง
        /// </summary>
        public const int INPUT_ERROR = -2;
        /// <summary>
        /// ถูก locked การใช้งาน
        /// </summary>
        public const int CAR_LOCKED = -3;
        /// <summary>
        /// มีรถคันนี้แล้ว
        /// </summary>
        public const int CAR_DUPLICATE = -4;
    }

    public class User
    {
        /// <summary>
        /// มี user นี้ในระบบแล้ว
        /// </summary>
        public const int USER_DUPLICATE = -1;
        /// <summary>
        /// ข้อมูล input ไม่ถูกต้อง
        /// </summary>
        public const int INPUT_ERROR = -2;
        /// <summary>
        /// ยังไม่ได้ยืนยันตัวตน
        /// </summary>
        public const int USER_NO_ACTIVATED  = -3;
        /// <summary>
        /// ถูก locked การใช้งาน
        /// </summary>
        public const int USER_LOCKED = -4;
        /// <summary>
        /// ไม่พบข้อมูบผู้ใช้งาน
        /// </summary>
        public const int NO_DATA_USER = -5;
    }

    public class MChauffeurCar
    {
        /// <summary>
        /// ข้อมูล input ไม่ถูกต้อง
        /// </summary>
        public const int INPUT_ERROR = -2;
        /// <summary>
        /// หาคนขับไม่เจอ หรือคนขับถูก locked
        /// </summary>
        public const int NO_DATA_CHAUFFEUE_OR_CAR_LOCKED = -3;
        /// <summary>
        /// ไม่พบข้อมูล
        /// </summary>
        public const int NO_DATA_MATCH = -4;
    }

    public class Job
    {
        /// <summary>
        /// 
        /// </summary>
        public const int INPUT_ERROR = -2;
        /// <summary>
        /// ไม่พบข้อมูล
        /// </summary>
        public const int NO_DATA_JOB = -5;
    }

    /// <summary>
    /// รหัสบริษัท
    /// </summary>
    public class Company
    {
        public const int DEMO = 1;
    }

    public class log_user_type
    {
        public const int USER_ONLINE = 1;
        public const int USER_OFFLINE = 2;
        public const int USER_POSITION = 3;
        public const int CHAUFFEUE_POSITION = 4;
    }

    public class ErrorCode
    {
        public const int SUCCESS = 1;
        public const int ERROR_STATUS_ONLINE = -111; 
        public const int UNKNOWN = 0;
        public const int NO_INTERNET_CONNECTION = -10;

        public const int USER = -100;
        public const int CAR = -101;
        public const int CHAUFFEUR = -102;
        public const int JOB = -103;
        public const int MATCH_CHAUFFEUR_CAR = -104;
        public const int INTERNAL_ERROR = -1000;
    }
}